package com.unicorn.edu.tasklist.service;

import com.unicorn.edu.tasklist.repository.TaskRepository;
import com.unicorn.edu.tasklist.repository.entity.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    public void addTask(Task task) {
        taskRepository.save(task);
    }

    public int getTasksCount() {
        return taskRepository.getAllTasks().size();
    }

    public void printAllTasks() {
        for (Task task : taskRepository.getAllTasks()) {
            System.out.println(task);
        }
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }
}
