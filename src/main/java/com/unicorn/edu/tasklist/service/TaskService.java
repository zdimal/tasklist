package com.unicorn.edu.tasklist.service;

import com.unicorn.edu.tasklist.repository.entity.Task;

public interface TaskService {

    void addTask(Task task);

    int getTasksCount();

    void printAllTasks();

}
