package com.unicorn.edu.tasklist.repository;

import com.unicorn.edu.tasklist.repository.entity.Task;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class TaskRepositoryImpl implements TaskRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public Task getTaskById(Long id) {
        Assert.notNull(id, "The input parameter id is null.");
        return null;
    }

    public List<Task> getAllTasks() {
        return entityManager.createQuery("from Task", Task.class).getResultList();
    }

    public void save(Task task) {
        if (task.getId() == null) {
            entityManager.persist(task);
        } else {
            entityManager.merge(task);
        }
    }

    public void save(List<Task> tasks) {

    }
}
