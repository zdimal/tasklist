package com.unicorn.edu.tasklist.repository.entity;

public enum Status {

    NEW, INPROGRESS, DONE
}
