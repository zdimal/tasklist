package com.unicorn.edu.tasklist.repository;

import com.unicorn.edu.tasklist.repository.entity.Task;

import java.util.List;

public interface TaskRepository {

    Task getTaskById(Long id);

    List<Task> getAllTasks();

    void save(Task task);

    void save(List<Task> tasks);

}
