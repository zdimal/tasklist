package com.unicorn.edu.tasklist;

import com.unicorn.edu.tasklist.repository.entity.Status;
import com.unicorn.edu.tasklist.repository.entity.Task;
import com.unicorn.edu.tasklist.service.TaskService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("metadata.xml");

        /*TaskRepository taskRepository = context.getBean(TaskRepository.class);

        List<Task> tasks = taskRepository.getAllTasks();

        for (Task task : tasks) {
            System.out.println(task);
        }*/

        Task task = new Task();
        task.setName("Abcd");
        task.setStatus(Status.NEW);

        TaskService taskService = context.getBean(TaskService.class);

        taskService.addTask(task);

        taskService.printAllTasks();

    }

}
