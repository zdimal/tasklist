package com.unicorn.edu.tasklist.repository;

import com.unicorn.edu.tasklist.repository.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepositoryMock implements TaskRepository {

    private List<Task> tasks;

    public Task getTaskById(Long id) {
        return null;
    }

    public List<Task> getAllTasks() {
        if (tasks == null) {
            prepareMockTasks();
        }
        return tasks;
    }

    public void save(Task task) {

    }

    public void save(List<Task> tasks) {

    }

    private void prepareMockTasks() {
        tasks = new ArrayList<Task>();

        Task task1 = new Task();
        task1.setId(1L);
        task1.setName("První task");

        Task task2 = new Task();
        task2.setId(2L);
        task2.setName("Druhý task");

        tasks.add(task1);
        tasks.add(task2);
    }
}
