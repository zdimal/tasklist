package com.unicorn.edu.tasklist.repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/metadata-test.xml")
public class TaskRepositoryTest {

    @Autowired
    private TaskRepository taskRepository;

    @Test
    public void testAllTasksCount() {
        ApplicationContext context = new ClassPathXmlApplicationContext("metadata.xml");
        TaskRepository taskRepositoryIn = context.getBean(TaskRepository.class);
        Assert.assertEquals(1, taskRepositoryIn.getAllTasks().size());
    }

    @Test
    public void testTasksCount2() {
        Assert.assertEquals(2, taskRepository.getAllTasks().size());
    }

    @Test
    public void testResource() throws IOException {
        ApplicationContext context = new ClassPathXmlApplicationContext("metadata-test.xml");
        Resource resource = context.getResource("file:/Users/petr/test.csv");
        Assert.assertNotNull(resource.getInputStream());
        String content = StreamUtils.copyToString(resource.getInputStream(), Charset.defaultCharset());
        System.out.println(content);
    }

}
